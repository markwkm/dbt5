From dbt5-base

ENV PATH="/usr/pgsql-11/bin:$PATH"

COPY . /usr/local/src/dbt5
WORKDIR /usr/local/src/dbt5
RUN cmake -DDBMS=pgsql .
RUN make
RUN make install
