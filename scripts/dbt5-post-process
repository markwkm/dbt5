#!/bin/sh

#
# This file is released under the terms of the Artistic License.
# Please see the file LICENSE, included in this package, for details.
#
# Copyright (C) 2006      Open Source Development Labs, Inc.
#               2006      Rilson Nascimento.
#               2006-2021 Mark Wong
#

MIXFILE=$1

if [ $# -ne 1 ]; then
    echo "usage: `basename $0` <mix.log>"
    exit 1
fi

R --slave --no-save << __EOF__
sd <- 0
bv <- 1
cp <- 2
mw <- 3
ts <- 4
tl <- 5
to <- 6
tu <- 7
mf <- 8
tr <- 9
dm <- 10
tc <- 11

transaction_name <- c("Security Detail", "Broker Volume", "Customer Position",
                      "Market Watch", "Trade Status", "Trade Lookup",
                      "Trade Order", "Trade Update", "Market Feed",
                      "Trade Result", "Data Maintenance")

df <- read.csv("${MIXFILE}", header=F)

colnames(df)[1] <- 'ctime'
colnames(df)[2] <- 'txn'
colnames(df)[3] <- 'status'
colnames(df)[3] <- 'response'

rampup_start <- df[1,]\$ctime

start_subset <- subset(df, df\$txn == "START")

steadystate_start <- start_subset[1,]\$ctime
total_txn <- nrow(df)
steadystate_end <- df[total_txn:total_txn,]\$ctime
duration <- steadystate_end - steadystate_start
steadystate <- subset(df, df\$ctime > steadystate_start)

total_transaction_count <- nrow(subset(steadystate, steadystate\$txn != dm))

cat(sprintf("%39s\n", "Response Time"))
cat(sprintf("%37s\n", "(seconds)"))
cat(sprintf("%-17s       %% %s:%5s %% %7s %14s %7s %7s\n",
        "Transaction", "Average", "90th", "Total", "Rollbacks    %", "Warning",
        "Invalid"))
cat(paste("----------------- ------- --------------- ------- --------------",
	  "------- -------\n"), sep = "")
x <- c(tr, bv, cp, mf, mw, sd, tl, to, ts, tu, dm)
for (key in x) {
    if (key == dm) {
        next
    }
    transaction_count <- sum(steadystate\$txn == key)
    rollback_count <- sum(steadystate\$txn == key & steadystate\$status == 1)
    if (key == tr) {
        transaction_count_tr = transaction_count
        rollback_count_tr <- rollback_count
    }
    invalid_count <- sum(steadystate\$txn == key & steadystate\$status < 0)
    warning_count <- sum(steadystate\$txn == key & steadystate\$status > 1)
    per90 = quantile(steadystate\$response[steadystate\$txn == key], .9)
    average_rt = mean(steadystate\$response[steadystate\$txn == key])

    mix_percentage <- (transaction_count - rollback_count) / (total_transaction_count) * 100.0
    rollback_percentage <- rollback_count / transaction_count * 100.0

    cat(sprintf("%-17s %7.3f %7.3f:%7.3f %7d %6d %6.2f%% %7d %7d\n",
            transaction_name[key +1],
            mix_percentage,
            average_rt,
            per90,
            transaction_count,
            rollback_count,
            rollback_percentage,
            warning_count,
	    invalid_count))
}
cat(paste("----------------- ------- --------------- ------- --------------",
	  "------- -------\n"), sep = "")
cat(sprintf("%0.2f trade-result transactions per second (trtps)\n",
        (transaction_count_tr - rollback_count_tr) / duration))
cat(sprintf("%0.1f minute(s) to ramp up\n",
	(steadystate_start - rampup_start) / 60.0))
cat(sprintf("%0.1f minute steady state duration\n", duration / 60.0))
__EOF__
