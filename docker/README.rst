Docker can be used to containerize the driver side of the workload, which
is composed of:

* Customer Emulator (CE)
* Database
* Market-Exchange Emulator (MEE)
* EGenDriver

The fastest way to try things out is to run::

    start-database-pgsql
    start-driver-pgsql <database address>

The purpose of the following scripts are:

* build-database-pgsql: Build a container with a minimalize sized dbt5
                        database for PostgreSQL
* build-driver-pgsql: Build a container with the dbt5 driver for PostgreSQL
* prepare-image: Build a container image tagged "dbt5" that will be used by all
  other scripts.
* compile-egen-pgsql: Only for compiling EGen itself with PostgreSQL specific
                      classes.  This does not build the entire test kit.
* compile-dbt5: Build and install the entire test kit.
* compile-sp-pgsql: Only for compiling the user defined functions for PostgreSQL
* shell: Start one of the dbt5 tagged containers and open a shell.
* start-database-pgsql: Start the PostgreSQL database.
* start-driver-pgsql: Start a test with parameters set in `start-driver`
